package me.fmotta.challenges.samaiait.syscad;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * Classe de inicialização de testes unitários do SpringBoot
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@SpringBootTest
class SyscadApplicationTests {

}
