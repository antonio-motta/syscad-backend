-- --------------------------------------------------------
-- Criação da chave estrangeiras que cria o relacionamento 
-- da tabela de endereços para a pessoa.
-- --------------------------------------------------------
ALTER TABLE address ADD CONSTRAINT address_person_fk 
	FOREIGN KEY (person_id) REFERENCES person(id);