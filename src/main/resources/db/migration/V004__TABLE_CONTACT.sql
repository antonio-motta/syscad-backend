
-- --------------------------------------------------------
-- Criação da tabela de telefones
-- --------------------------------------------------------
CREATE TABLE contact (
	id UUID NOT NULL,
	person_id UUID NOT NULL,
	phone_number varchar(11) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT contact_pkey PRIMARY KEY (id)
);

-- --------------------------------------------------------
-- Criação da chave estrangeiras que cria o relacionamento 
-- da tabela de telefones para a pessoa.
-- --------------------------------------------------------
ALTER TABLE contact ADD CONSTRAINT contact_person_fk 
	FOREIGN KEY (person_id) REFERENCES person(id);