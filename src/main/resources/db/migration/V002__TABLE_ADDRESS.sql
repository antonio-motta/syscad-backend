-- --------------------------------------------------------
-- Criação da tabela de endereços
-- --------------------------------------------------------
CREATE TABLE address (
	id UUID NOT NULL,
	person_id UUID NOT NULL,
	street varchar(120) NOT NULL,
	cep varchar(8) NOT NULL,
	neighborhood varchar(80) not null,
	city varchar(50) not null,
	uf varchar(2) not null,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT address_pkey PRIMARY KEY (id)
);
