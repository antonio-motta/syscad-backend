-- -------------------------------------------------
-- Criação da tabela de pessoas
-- -------------------------------------------------
CREATE TABLE person (
	id UUID NOT NULL,
	name varchar(120) NOT NULL,
	cpf varchar(11) NOT NULL,
	email varchar(80) NULL,
	birthday date NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT student_pkey PRIMARY KEY (id)
);
