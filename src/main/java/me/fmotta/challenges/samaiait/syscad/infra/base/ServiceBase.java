package me.fmotta.challenges.samaiait.syscad.infra.base;

import java.util.List;
import java.util.UUID;

/**
 * otInteface base dos serviços
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public interface ServiceBase<T> {

	/**
	 * Salva a entidade na base
	 * 
	 * @param t entidade a ser salva.
	 * @return t entidade salva
	 */
	public T save(T t);

	/**
	 * Atualiza a entidade na base
	 * 
	 * @param t  entidade a ser atualizada.
	 * @param id identificação da entidade
	 * @return t entidade salva
	 */
	public T update(T t, UUID id);

	/**
	 * Busca todos os registros entidade
	 * 
	 * @param t  entidade a ser atualizada.
	 * @param id identifiodatodos os registros da entidade
	 * 
	 * @return List<T> todos os registros da entidade
	 */
	public List<T> findAll();

	/**
	 * Busca um registro da entidade pelo id
	 * 
	 * @param id identificação do registro
	 * @return T registro da entidade;
	 */
	public T findById(UUID id);

	/**
	 * Deleta todos os registros da entidade
	 */
	public void deleteAll();

	/**
	 * Deleta um registro da entidade pelo id
	 * 
	 * @param id identificação do registro da entidade
	 */
	public void deleteById(UUID id);
}
