package me.fmotta.challenges.samaiait.syscad.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import me.fmotta.challenges.samaiait.syscad.dto.AddressPayload;
import me.fmotta.challenges.samaiait.syscad.dto.ContactPayload;
import me.fmotta.challenges.samaiait.syscad.dto.PersonPayload;
import me.fmotta.challenges.samaiait.syscad.respository.address.AddressEntity;
import me.fmotta.challenges.samaiait.syscad.respository.address.AddressRepository;
import me.fmotta.challenges.samaiait.syscad.respository.contact.ContactEntity;
import me.fmotta.challenges.samaiait.syscad.respository.contact.ContactRepository;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonEntity;

/**
 * 
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Component
public class PersonMapper {

	@Autowired
	private AddressMapper addressMapper;

	@Autowired
	private ContactMapper contactMapper;

	@Autowired
	private ContactRepository contactRepository;

	@Autowired
	private AddressRepository addressRepository;

	private PersonMapper() {
	}

	/**
	 * Converte {@link PersonPayload} para {@link PersonEntity}
	 * 
	 * @param payload carga de dados.
	 * @return {@link PersonEntity} entidade.
	 */
	public PersonEntity toEntity(PersonPayload payload) {
		PersonEntity entity = new PersonEntity();
		if (payload.getId() != null && !payload.getId().equals(""))
			entity.setId(UUID.fromString(payload.getId()));
		entity.setName(payload.getName());
		entity.setCpf(payload.getCpf());
		entity.setEmail(payload.getEmail());
		entity.setBirthday(payload.getBirthday());
		List<AddressEntity> address = payload.getAddress().stream()//
				.map(a -> addressMapper.toEntity(a, entity)).collect(Collectors.toList());
		entity.setAddress(address);
		List<ContactEntity> contacts = payload.getContacts().stream()//
				.map(c -> contactMapper.toEntity(c, entity)).collect(Collectors.toList());
		entity.setContacts(contacts);
		return entity;
	}

	/**
	 * Atualiza {@link PersonPayload} para {@link PersonEntity}
	 * 
	 * @param payload carga de dados.
	 * @param entity  entidade.
	 */
	public void merge(PersonPayload payload, PersonEntity entity) {
		entity.setId(UUID.fromString(payload.getId()));
		entity.setName(payload.getName());
		entity.setCpf(payload.getCpf());
		entity.setEmail(payload.getEmail());
		entity.setBirthday(payload.getBirthday());
		List<ContactEntity> contacts = new ArrayList<>();
		for (ContactPayload c : payload.getContacts()) {
			Optional<ContactEntity> phoneOptional = !c.getId().equals("")
					? contactRepository.findById(UUID.fromString(c.getId()))
					: Optional.empty();
			ContactEntity phone;
			if (phoneOptional.isPresent()) {
				phone = phoneOptional.get();
				contactMapper.merge(phone, c);
			} else {
				phone = contactMapper.toEntity(c, entity);
			}
			contacts.add(phone);
		}
		entity.getContacts().clear();
		entity.getContacts().addAll(contacts);
		List<AddressEntity> addresses = new ArrayList<>();
		for (AddressPayload a : payload.getAddress()) {
			Optional<AddressEntity> addressOptional = !a.getId().equals("")
					? addressRepository.findById(UUID.fromString(a.getId()))
					: Optional.empty();
			AddressEntity address;
			if (addressOptional.isPresent()) {
				address = addressOptional.get();
				addressMapper.merge(address, a);
			} else {
				address = addressMapper.toEntity(a, entity);
			}
			addresses.add(address);
		}
		entity.getAddress().clear();
		entity.getAddress().addAll(addresses);

	}

	/**
	 * Converte {@link PersonEntity} para {@link PersonPayload}
	 * 
	 * @param payload carga de dados.
	 * @return {@link PersonPayload} entidade.
	 */
	public PersonPayload toPayload(PersonEntity entity) {
		PersonPayload payload = new PersonPayload();
		if (entity.getId() != null)
			payload.setId(entity.getId().toString());
		payload.setName(entity.getName());
		payload.setCpf(entity.getCpf());
		payload.setEmail(entity.getEmail());
		payload.setBirthday(entity.getBirthday());
		payload.setCreatedAt(entity.getCreatedAt());
		List<AddressPayload> address = entity.getAddress().stream()//
				.map(a -> addressMapper.toPayload(a)).collect(Collectors.toList());
		payload.setAddress(address);
		List<ContactPayload> contacts = entity.getContacts().stream()//
				.map(c -> contactMapper.toPayload(c)).collect(Collectors.toList());
		payload.setContacts(contacts);
		return payload;
	}

	/**
	 * Atualiza {@link PersonEntity} para {@linkPersonPayload}
	 * 
	 * @param payload carga de dados.
	 * @param entity  entidade.
	 */
	public void merge(PersonEntity entity, PersonPayload payload) {
		payload.setId(entity.getId().toString());
		payload.setName(entity.getName());
		payload.setCpf(entity.getCpf());
		payload.setEmail(entity.getEmail());
		payload.setBirthday(entity.getBirthday());
		List<AddressPayload> address = entity.getAddress().stream()//
				.map(a -> addressMapper.toPayload(a)).collect(Collectors.toList());
		payload.setAddress(address);
		List<ContactPayload> contacts = entity.getContacts().stream()//
				.map(c -> contactMapper.toPayload(c)).collect(Collectors.toList());
		payload.setContacts(contacts);
	}
}
