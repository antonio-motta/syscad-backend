package me.fmotta.challenges.samaiait.syscad.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.fmotta.challenges.samaiait.syscad.dto.PersonPayload;
import me.fmotta.challenges.samaiait.syscad.infra.base.ServiceBase;
import me.fmotta.challenges.samaiait.syscad.mapper.PersonMapper;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonEntity;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonRepository;

/**
 * Serviços da entidade pessoa.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Service
@Transactional
public class PersonService implements ServiceBase<PersonPayload> {

	@Autowired
	PersonMapper mapper;

	@Autowired
	PersonRepository repository;

	@Override
	public PersonPayload save(PersonPayload dto) {
		PersonEntity entity = mapper.toEntity(dto);
		return mapper.toPayload(repository.saveAndFlush(entity));
	}

	@Override
	public PersonPayload update(PersonPayload dto, UUID id) {
		Optional<PersonEntity> person = repository.findById(id);
		if (person.isPresent()) {
			PersonEntity entity = person.get();
			mapper.merge(dto, entity);
			return mapper.toPayload(repository.saveAndFlush(entity));
		}
		return null;
	}

	@Override
	public List<PersonPayload> findAll() {
		List<PersonEntity> all = repository.findAll();
		return all.stream().map(p -> mapper.toPayload(p)).collect(Collectors.toList());
	}

	@Override
	public PersonPayload findById(UUID id) {
		Optional<PersonEntity> person = repository.findById(id);
		return person.isPresent() ? mapper.toPayload(person.get()) : null;
	}

	/**
	 * Serviço de busca inteligente por nome, email ou cpf.
	 * 
	 * @param keyword palavra chave.
	 * @return {@link List<PersonEntity>} lista de pessoas.
	 */
	public List<PersonPayload> findSmartByKeyword(String keyword) {
		List<PersonEntity> all = repository.smartSearchByKeyword(keyword);
		return all.stream().map(p -> mapper.toPayload(p)).collect(Collectors.toList());
	}

	@Override
	public void deleteAll() {
		repository.deleteAll();

	}

	@Override
	public void deleteById(UUID id) {
		repository.deleteById(id);
	}

}
