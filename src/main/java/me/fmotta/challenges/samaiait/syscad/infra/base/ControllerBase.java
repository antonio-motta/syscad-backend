package me.fmotta.challenges.samaiait.syscad.infra.base;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
/**
 * Intefarce base dos controladores
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public interface ControllerBase<T> {

	/**
	 * Salva a entidade na base
	 * 
	 * @param t entidade a ser salva.
	 * @return t entidade salva
	 */
	@PostMapping
	public ResponseEntity<T> save(@RequestBody @Valid T t);

	/**
	 * Atualiza a entidade na base
	 * 
	 * @param t  entidade a ser atualizada.
	 * @param id identificação da entidade
	 * @return t entidade salva
	 */
	@PutMapping("/id/{id}")
	public ResponseEntity<T> update(@PathVariable("id") UUID id, @RequestBody @Valid T t);

	/**
	 * Busca todos os registros entidade
	 * 
	 * @param t  entidade a ser atualizada.
	 * @param id identifiodatodos os registros da entidade
	 * 
	 * @return List<T> todos os registros da entidade
	 */
	@GetMapping
	public ResponseEntity<List<T>> findAll();

	/**
	 * Busca um registro da entidade pelo id
	 * 
	 * @param id identificação do registro
	 * @return T registro da entidade;
	 */
	@GetMapping("/id/{id}")
	public ResponseEntity<T> findById(@PathVariable("id") UUID id);

	/**
	 * Deleta todos os registros da entidade
	 */
	@DeleteMapping
	public ResponseEntity<T> deleteAll();

	/**
	 * Deleta um registro da entidade pelo id
	 * 
	 * @param id identificação do registro da entidade
	 */
	@DeleteMapping("/id/{id}")
	public ResponseEntity<T> deleteById(@PathVariable("id") UUID id);
}
