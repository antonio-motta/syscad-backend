package me.fmotta.challenges.samaiait.syscad.infra.base;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * Repositorio de implementação base com implemtação de selects elegantes com
 * QueryDSL.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class RepositoryCustomImplBase {

	/**
	 * {@link EntityManager} gerenciador de clicos de vidas de entidades.
	 */
	@PersistenceContext
	protected EntityManager em;

	/**
	 * Busca de entidade com {@link JPAQuery}
	 * 
	 * @param <U>  Classe entide
	 * @param expr query jpa
	 * @return resultado da consulta.
	 */
	public <U> JPAQuery<U> select(Expression<U> expr) {
		return new JPAQuery<>(em).select(expr);
	}

	/**
	 * Busca de entidade com tuplas {@link JPAQuery}
	 * 
	 * @param exprs query jpa
	 * @return resultado da consulta como tupla.
	 */
	public JPAQuery<Tuple> select(Expression<?>... exprs) {
		return new JPAQuery<>(em).select(exprs);
	}
}
