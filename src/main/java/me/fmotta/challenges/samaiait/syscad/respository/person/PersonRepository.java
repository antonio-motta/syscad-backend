package me.fmotta.challenges.samaiait.syscad.respository.person;

import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryBase;

/**
 * Repositório da entidade {@link PersonEntity}
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Repository
public interface PersonRepository extends RepositoryBase<PersonEntity>, PersonRepositoryCustom {

	/**
	 * Busca pessoa pelo id.
	 */
	public Optional<PersonEntity> findById(UUID id);

}
