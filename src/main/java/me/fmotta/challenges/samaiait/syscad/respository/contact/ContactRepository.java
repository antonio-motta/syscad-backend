package me.fmotta.challenges.samaiait.syscad.respository.contact;

import org.springframework.stereotype.Repository;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryBase;

/**
 * Repositório da entidade {@link ContactEntity}
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Repository
public interface ContactRepository extends RepositoryBase<ContactEntity>, ContactRepositoryCustom {

}
