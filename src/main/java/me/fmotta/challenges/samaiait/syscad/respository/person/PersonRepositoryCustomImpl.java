package me.fmotta.challenges.samaiait.syscad.respository.person;

import java.util.List;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryCustomImplBase;

/**
 * Implementação dos métodos da intefarce {@link PersonRepositoryCustom}
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class PersonRepositoryCustomImpl extends RepositoryCustomImplBase implements PersonRepositoryCustom {

	@Override
	public List<PersonEntity> smartSearchByKeyword(String keyword) {
		QPersonEntity person = QPersonEntity.personEntity;
		OrderSpecifier<?> sort = person.name.asc();
		JPAQuery<PersonEntity> query = select(person).from(person)//
				.orderBy(sort);
		filterSmartSearchByKeyword(keyword, person, query);

		return query.fetch();
	}

	/**
	 * filtro de busca inteligente por nome, email ou cpf.
	 * 
	 * @param keyword palavra-chave
	 * @param person  Entidade querydsl
	 * @param query   query jpa a ser acrescentanda a expressão de busca.
	 */
	private void filterSmartSearchByKeyword(String keyword, QPersonEntity person, JPAQuery<PersonEntity> query) {
		keyword = keyword.replaceAll("\\s+", " ").trim();

		String[] strings = keyword.split("\\s");
		for (String str : strings) {
			BooleanExpression terms = person.name.containsIgnoreCase(str) //
					.or(person.email.containsIgnoreCase(str)) //
					.or(person.cpf.containsIgnoreCase(str));
			query.where(terms);
		}
	}
}
