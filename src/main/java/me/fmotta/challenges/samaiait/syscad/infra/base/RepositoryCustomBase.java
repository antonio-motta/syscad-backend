package me.fmotta.challenges.samaiait.syscad.infra.base;

/**
 * Interface de repositorio base do métodos de queryDsl.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public interface RepositoryCustomBase<T> {

}
