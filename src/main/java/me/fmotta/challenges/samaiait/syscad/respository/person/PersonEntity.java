package me.fmotta.challenges.samaiait.syscad.respository.person;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import me.fmotta.challenges.samaiait.syscad.infra.base.EntityBase;
import me.fmotta.challenges.samaiait.syscad.respository.address.AddressEntity;
import me.fmotta.challenges.samaiait.syscad.respository.contact.ContactEntity;

/**
 * Entidade de pessoa.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Entity
@Table(name = "PERSON")
public class PersonEntity extends EntityBase {

	/**
	 * Serialização padrão.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nome da pessoa.
	 */
	@Column(name = "name")
	private String name;

	/**
	 * CPF da pessoa.
	 */
	@Column(name = "cpf")
	private String cpf;

	/**
	 * Email da pessoa.
	 */
	@Column(name = "EMAIL")
	private String email;

	/**
	 * Data de nascimento da pessoa.
	 */
	@Column(name = "BIRTHDAY")
	private Date birthday;

	/**
	 * Endereços.
	 */
	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "person")
	private List<AddressEntity> address = new ArrayList<>();

	/**
	 * Telefones.
	 */
	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "person")
	private List<ContactEntity> contacts = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public List<AddressEntity> getAddress() {
		return address;
	}

	public void setAddress(List<AddressEntity> address) {
		this.address = address;
	}

	public List<ContactEntity> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContactEntity> contacts) {
		this.contacts = contacts;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(address, birthday, contacts, cpf, email, name);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonEntity other = (PersonEntity) obj;
		return Objects.equals(address, other.address) && Objects.equals(birthday, other.birthday)
				&& Objects.equals(contacts, other.contacts) && Objects.equals(cpf, other.cpf)
				&& Objects.equals(email, other.email) && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "PersonEntity [name=" + name + ", cpf=" + cpf + ", email=" + email + ", birthday=" + birthday
				+ ", address=" + address + ", contacts=" + contacts + "]";
	}

}
