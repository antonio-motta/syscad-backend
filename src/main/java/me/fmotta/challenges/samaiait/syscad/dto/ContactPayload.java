package me.fmotta.challenges.samaiait.syscad.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe dto de carga de telefones.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class ContactPayload implements Serializable {

	/**
	 * Serialização padrão
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador unico.
	 */
	@JsonProperty("id")
	private String id;

	/**
	 * UF do endereço.
	 */
	@JsonProperty("phone")
	private String phoneNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, phoneNumber);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactPayload other = (ContactPayload) obj;
		return Objects.equals(id, other.id) && Objects.equals(phoneNumber, other.phoneNumber);
	}

	@Override
	public String toString() {
		return "ContactPayload [id=" + id + ", phoneNumber=" + phoneNumber + "]";
	}

}