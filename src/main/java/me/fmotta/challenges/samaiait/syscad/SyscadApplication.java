package me.fmotta.challenges.samaiait.syscad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Classe de inicialização da aplicação pelo SpringBoot
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@EnableJpaAuditing
@SpringBootApplication
public class SyscadApplication {

	/**
	 * Método de inicialização principal
	 * 
	 * @param args paramentros de inicialização se necessário.
	 */
	public static void main(String[] args) {
		SpringApplication.run(SyscadApplication.class, args);
	}

}