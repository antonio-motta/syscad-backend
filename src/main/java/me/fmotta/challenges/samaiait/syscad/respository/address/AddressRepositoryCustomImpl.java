package me.fmotta.challenges.samaiait.syscad.respository.address;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryCustomImplBase;

/**
 * Implementação dos métodos da intefarce {@link AddressRepositoryCustom}
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class AddressRepositoryCustomImpl extends RepositoryCustomImplBase implements AddressRepositoryCustom {

}
