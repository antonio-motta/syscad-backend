package me.fmotta.challenges.samaiait.syscad.infra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configurações de documentação Swagger
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	/**
	 * Pacote onde estão os endpoints a serem documentados
	 */
	private static final String BASE_PACKAGE = "me.fmotta.challenges.samaiait.syscad.controller";

	/**
	 * Titulo que será apresentado na pagina gerada pelo Swagger
	 */
	private static final String TITLE = "Point Management - DOCS";

	/**
	 * Descrição que será apresentada na pagina gerada pelo Swagger
	 */
	private static final String DESCRIPTION = "Documentação de API";

	/**
	 * Versão que será apresentada na pagina gerada pelo Swagger
	 */
	private static final String VERSION = "1.0.0";

	/**
	 * Licença que será apresentada pagina gerada pelo Swagger
	 */
	private static final String LICENSE = "MIT Licence";

	/**
	 * URL da licença que será apresentada pagina gerada pelo Swagger
	 */
	private static final String LICENSE_URL = "https://github.com/antonio-motta/syscad/blob/master/LICENSE";

	/**
	 * Nome de contato que será apresentado pagina gerada pelo Swagger
	 */
	private static final String CONTACT_NAME = "Motta, Antonio F. Farias";

	/**
	 * URL de contato que será apresentada pagina gerada pelo Swagger
	 */
	private static final String CONTACT_URL = "https://www.linkedin.com/in/affmotta";

	/**
	 * Email de contato que será apresentado pagina gerada pelo Swagger
	 */
	private static final String CONTACT_EMAIL = "fmotta.antfco@gmail.com";

	/**
	 * Especificando pacote que contém apis a serem documentadas.
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2) //
				.select().apis(RequestHandlerSelectors //
						.basePackage(BASE_PACKAGE)) //
				.paths(PathSelectors.any()).build() //
				.apiInfo(apiInfo());
	}

	/**
	 * Customização UI do Swagger.
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(TITLE) //
				.description(DESCRIPTION).version(VERSION) //
				.license(LICENSE) //
				.licenseUrl(LICENSE_URL) //
				.contact(new Contact(CONTACT_NAME, CONTACT_URL, CONTACT_EMAIL)) //
				.build();
	}
}
