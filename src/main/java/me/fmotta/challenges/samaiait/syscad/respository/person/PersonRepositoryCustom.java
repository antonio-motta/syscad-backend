package me.fmotta.challenges.samaiait.syscad.respository.person;

import java.util.List;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryCustomBase;

/**
 * Repositório da entidade {@link PersonEntity} que não utilizam springdata.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */

public interface PersonRepositoryCustom extends RepositoryCustomBase<PersonEntity> {

	/**
	 * Busca inteligente por nome, email ou cpf.
	 * 
	 * @param keyword palavra chave
	 * @return {@link List<PersonEntity>} lista de pessoas.
	 */
	public List<PersonEntity> smartSearchByKeyword(String keyword);

}
