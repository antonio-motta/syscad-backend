package me.fmotta.challenges.samaiait.syscad.respository.contact;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import me.fmotta.challenges.samaiait.syscad.infra.base.EntityBase;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonEntity;

/**
 * Entidade de telefones
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Entity
@Table(name = "CONTACT")
public class ContactEntity extends EntityBase {

	/**
	 * Serialização padrão.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Entidade pessoa.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PERSON_ID", referencedColumnName = "ID", nullable = false)
	private PersonEntity person;

	/**
	 * Logradouro.
	 */
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(person, phoneNumber);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactEntity other = (ContactEntity) obj;
		return Objects.equals(person, other.person) && Objects.equals(phoneNumber, other.phoneNumber);
	}

	@Override
	public String toString() {
		return "ContactEntity [person=" + person + ", phoneNumber=" + phoneNumber + "]";
	}

}
