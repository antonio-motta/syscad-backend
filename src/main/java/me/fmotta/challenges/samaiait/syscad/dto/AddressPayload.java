package me.fmotta.challenges.samaiait.syscad.dto;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe dto de carga de endereços.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class AddressPayload implements Serializable {

	/**
	 * Serialização padrão
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Indentificador único.
	 */
	@JsonProperty("id")
	private String id;

	/**
	 * Logradouro do endereço.
	 */
	@JsonProperty("street")
	private String street;

	/**
	 * Cep do endereço.
	 */
	@JsonProperty("cep")
	private String cep;

	/**
	 * Bairro do endereço.
	 */
	@JsonProperty("neighborhood")
	private String neighborhood;

	/**
	 * Cidade do endereço.
	 */
	@JsonProperty("city")
	private String city;

	/**
	 * UF do endereço.
	 */
	@JsonProperty("uf")
	private String uf;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public String getCep() {
		return cep;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public String getCity() {
		return city;
	}

	public String getUf() {
		return uf;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cep, city, id, neighborhood, street, uf);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressPayload other = (AddressPayload) obj;
		return Objects.equals(cep, other.cep) && Objects.equals(city, other.city) && Objects.equals(id, other.id)
				&& Objects.equals(neighborhood, other.neighborhood) && Objects.equals(street, other.street)
				&& Objects.equals(uf, other.uf);
	}

	@Override
	public String toString() {
		return "AddressPayload [id=" + id + ", street=" + street + ", cep=" + cep + ", neighborhood=" + neighborhood
				+ ", city=" + city + ", uf=" + uf + "]";
	}

}