package me.fmotta.challenges.samaiait.syscad.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.fmotta.challenges.samaiait.syscad.dto.PersonPayload;
import me.fmotta.challenges.samaiait.syscad.infra.base.ControllerBase;
import me.fmotta.challenges.samaiait.syscad.infra.config.LoggerSyscad;
import me.fmotta.challenges.samaiait.syscad.service.PersonService;

/**
 * Controlador da entidade pessoa.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@RestController
@RequestMapping("v1/person")
public class PersonController implements ControllerBase<PersonPayload> {

	@Autowired
	PersonService service;

	@Override
	public ResponseEntity<PersonPayload> save(@Valid PersonPayload dto) {
		try {
			PersonPayload save = service.save(dto);
			return dto != null ? ResponseEntity.ok(save) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@Override
	public ResponseEntity<PersonPayload> update(UUID id, @Valid PersonPayload dto) {
		try {
			PersonPayload update = service.update(dto, id);
			return update != null ? ResponseEntity.ok(update) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			LoggerSyscad.error(e.getMessage());
			return ResponseEntity.badRequest().build();
		}
	}

	@Override
	public ResponseEntity<List<PersonPayload>> findAll() {
		try {
			List<PersonPayload> findAll = service.findAll();
			return !findAll.isEmpty() ? ResponseEntity.ok(findAll) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@Override
	public ResponseEntity<PersonPayload> findById(UUID id) {
		try {
			PersonPayload findById = service.findById(id);
			return findById != null ? ResponseEntity.ok(findById) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	/**
	 * Controlador de busca inteligente por nome, email ou cpf.
	 * 
	 * @param keyword palavra chave.
	 * @return {@link List<PersonEntity>} lista de pessoas.
	 */
	@GetMapping("/keyword/{keyword}")
	public ResponseEntity<List<PersonPayload>> findSmartByKeyword(@PathVariable("keyword") String keyword) {
		try {
			List<PersonPayload> findSmart = service.findSmartByKeyword(keyword);
			return !findSmart.isEmpty() ? ResponseEntity.ok(findSmart) : ResponseEntity.notFound().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@Override
	public ResponseEntity<PersonPayload> deleteAll() {
		try {
			service.deleteAll();
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@Override
	public ResponseEntity<PersonPayload> deleteById(UUID id) {
		try {
			service.deleteById(id);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

}
