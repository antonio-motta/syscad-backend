package me.fmotta.challenges.samaiait.syscad.mapper;

import java.util.UUID;

import org.springframework.stereotype.Component;

import me.fmotta.challenges.samaiait.syscad.dto.AddressPayload;
import me.fmotta.challenges.samaiait.syscad.respository.address.AddressEntity;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonEntity;

/**
 * Conversor da classe de carga para entidade e vice-versa do endereço.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Component
public class AddressMapper {

	private AddressMapper() {
	}

	/**
	 * Converte {@link AddressPayload} para {@link AddressEntity}
	 * 
	 * @param payload carga.
	 * @return {@link AddressEntity} entidade.
	 */
	public AddressEntity toEntity(AddressPayload payload, PersonEntity person) {
		AddressEntity entity = new AddressEntity();
		if (payload.getId() != null && !payload.getId().equals(""))
			entity.setId(UUID.fromString(payload.getId()));
		entity.setPerson(person);
		entity.setStreet(payload.getStreet());
		entity.setCep(payload.getCep());
		entity.setNeighborhood(payload.getNeighborhood());
		entity.setCity(payload.getCep());
		entity.setUf(payload.getUf());
		return entity;
	}

	/**
	 * Converte {@link AddressPayload} para {@link AddressEntity}
	 * 
	 * @param payload carga com os dados.
	 * @param entity  entidade que receberar os dados.
	 */
	public void merge(AddressPayload payload, AddressEntity entity, PersonEntity person) {
		entity.setId(UUID.fromString(payload.getId()));
		entity.setPerson(person);
		entity.setStreet(payload.getStreet());
		entity.setCep(payload.getCep());
		entity.setNeighborhood(payload.getNeighborhood());
		entity.setCity(payload.getCep());
		entity.setUf(payload.getUf());
	}

	/**
	 * Converte {@link AddressEntity} para {@link AddressPayload}
	 * 
	 * @param payload carga.
	 * @return {@link AddressPayload} payload.
	 */
	public AddressPayload toPayload(AddressEntity entity) {
		AddressPayload payload = new AddressPayload();
		if (entity.getId() != null)
			payload.setId(entity.getId().toString());
		payload.setStreet(entity.getStreet());
		payload.setCep(entity.getCep());
		payload.setNeighborhood(entity.getNeighborhood());
		payload.setCity(entity.getCep());
		payload.setUf(entity.getUf());
		return payload;
	}

	/**
	 * Converte {@link AddressEntity} para {@link AddressPayload}
	 * 
	 * @param entity  carga com os dados.
	 * @param payload que receberar os dados.
	 */
	public void merge(AddressEntity entity, AddressPayload payload) {
		payload.setId(entity.getId().toString());
		payload.setStreet(entity.getStreet());
		payload.setCep(entity.getCep());
		payload.setNeighborhood(entity.getNeighborhood());
		payload.setCity(entity.getCep());
		payload.setUf(entity.getUf());
	}

}
