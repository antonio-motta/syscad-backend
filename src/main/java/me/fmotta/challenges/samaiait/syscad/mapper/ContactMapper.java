package me.fmotta.challenges.samaiait.syscad.mapper;

import java.util.UUID;

import org.springframework.stereotype.Component;

import me.fmotta.challenges.samaiait.syscad.dto.ContactPayload;
import me.fmotta.challenges.samaiait.syscad.respository.contact.ContactEntity;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonEntity;

/**
 * Conversor da classe de carga para entidade e vice-versa do telefones. 
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Component
public class ContactMapper {
	private ContactMapper() {
	}

	/**
	 * Converte {@link ContactPayload} para {@link ContactEntity}
	 * 
	 * @param payload carga.
	 * @return {@link ContactEntity} entidade.
	 */
	public ContactEntity toEntity(ContactPayload payload, PersonEntity person) {
		ContactEntity entity = new ContactEntity();
		if (payload.getId() != null && !payload.getId().equals(""))
			entity.setId(UUID.fromString(payload.getId()));
		entity.setPerson(person);
		entity.setPhoneNumber(payload.getPhoneNumber());
		return entity;
	}
	
		

	/**
	 * Atualiza a {@link ContactEntity} para {@link ContactPayload}
	 * 
	 * @param payload carga com os dados
	 * @param entity  quem recebe os dados.
	 */
	public void merge(ContactPayload payload, ContactEntity entity) {
		entity.setPhoneNumber(payload.getPhoneNumber());
	}

	/**
	 * Converte {@link ContactEntity} para {@link ContactPayload}
	 * 
	 * @param entity carga.
	 * @return {@link ContactPayload} payload.
	 */
	public ContactPayload toPayload(ContactEntity entity) {
		ContactPayload payload = new ContactPayload();
		if (entity.getId() != null)
			payload.setId(entity.getId().toString());
		payload.setPhoneNumber(entity.getPhoneNumber());
		return payload;
	}

	/**
	 * Atualiza a {@link ContactPayload} para {@link ContactEntity}
	 * 
	 * @param entity  carga com os dados
	 * @param payload quem recebe os dados.
	 */
	public void merge(ContactEntity entity, ContactPayload payload) {
		payload.setId(entity.getId().toString());
		payload.setPhoneNumber(entity.getPhoneNumber());
	}
}
