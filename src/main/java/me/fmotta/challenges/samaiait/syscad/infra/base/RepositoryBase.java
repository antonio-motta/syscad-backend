package me.fmotta.challenges.samaiait.syscad.infra.base;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * Interface de repositorio base.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public interface RepositoryBase<T extends EntityBase> extends JpaRepository<T, UUID>, QuerydslPredicateExecutor<T> {

}
