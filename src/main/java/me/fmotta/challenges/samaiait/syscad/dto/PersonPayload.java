package me.fmotta.challenges.samaiait.syscad.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe de carga para pessoa.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class PersonPayload implements Serializable {

	/**
	 * Serialização padrão.
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("id")
	private String id;

	@JsonProperty("name")
	private String name;

	@JsonProperty("cpf")
	private String cpf;

	@JsonProperty("email")
	private String email;

	@JsonProperty("birthday")
	private Date birthday;

	@JsonProperty("address")
	private List<AddressPayload> address;

	@JsonProperty("phones")
	private List<ContactPayload> contacts;
	
	@JsonProperty("createdAt")
	private Date createdAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public List<AddressPayload> getAddress() {
		return address;
	}

	public void setAddress(List<AddressPayload> address) {
		this.address = address;
	}

	public List<ContactPayload> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContactPayload> contacts) {
		this.contacts = contacts;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public int hashCode() {
		return Objects.hash(address, birthday, contacts, cpf, email, id, name, createdAt);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonPayload other = (PersonPayload) obj;
		return Objects.equals(address, other.address) && Objects.equals(birthday, other.birthday)
				&& Objects.equals(contacts, other.contacts) && Objects.equals(cpf, other.cpf)
				&& Objects.equals(email, other.email) && Objects.equals(id, other.id)
				&& Objects.equals(name, other.name) && Objects.equals(createdAt, other.createdAt);
	}

	@Override
	public String toString() {
		return "PersonPayload [id=" + id + ", name=" + name + ", cpf=" + cpf + ", email=" + email + ", birthday="
				+ birthday + ", address=" + address + ", contacts=" + contacts + ", contacts=" + contacts + "]";
	}

}
