package me.fmotta.challenges.samaiait.syscad.infra.base;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Base para DTO usado nos controladores
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class DTOBase implements Serializable {

	/**
	 * Serialização padrão.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificação Unica do DTO
	 */
	private UUID id;

	/**
	 * Atributo usado para auditoria
	 * 
	 * Nome do criador
	 */
	private String createdBy;

	/**
	 * Atributo usado para auditoria
	 * 
	 * Data de criação
	 */
	private Date createdAt;

	/**
	 * Atributo usado para auditoria
	 * 
	 * Nome do usuario que atualizou aplicação
	 */
	private String updatedBy;

	/**
	 * Atributo usado para auditoria
	 * 
	 * Data de Atualização
	 */
	private Date updatedAt;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public int hashCode() {
		return Objects.hash(createdAt, createdBy, id, updatedAt, updatedBy);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DTOBase other = (DTOBase) obj;
		return Objects.equals(createdAt, other.createdAt) && Objects.equals(createdBy, other.createdBy)
				&& Objects.equals(id, other.id) && Objects.equals(updatedAt, other.updatedAt)
				&& Objects.equals(updatedBy, other.updatedBy);
	}

	@Override
	public String toString() {
		return "BaseDTO [id=" + id + ", createdBy=" + createdBy + ", createdAt=" + createdAt + ", updatedBy="
				+ updatedBy + ", updatedAt=" + updatedAt + "]";
	}
}
