package me.fmotta.challenges.samaiait.syscad.respository.address;

import org.springframework.stereotype.Repository;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryBase;

/**
 * Repositório da entidade {@link PersonEntity}
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Repository
public interface AddressRepository extends RepositoryBase<AddressEntity>, AddressRepositoryCustom {

}
