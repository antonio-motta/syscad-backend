package me.fmotta.challenges.samaiait.syscad.respository.contact;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryCustomBase;

/**
 * Repositório da entidade {@link ContactEntity} que não utilizam springdata.
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */

public interface ContactRepositoryCustom extends RepositoryCustomBase<ContactEntity> {

}
