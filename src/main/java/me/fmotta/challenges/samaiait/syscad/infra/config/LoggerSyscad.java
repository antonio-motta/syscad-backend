package me.fmotta.challenges.samaiait.syscad.infra.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logger padrão da aplicação
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class LoggerSyscad {

	/**
	 * Construtor privado
	 */
	private LoggerSyscad() {
	}

	/**
	 * Inicialização do log
	 */
	private static final Logger logger = LoggerFactory.getLogger(LoggerSyscad.class);

	/**
	 * Logo de informação
	 * 
	 * @param info mensagem do log
	 */
	public static void info(String info) {
		logger.info(info);
	}

	/**
	 * Log de error
	 * 
	 * @param error mensagem do log
	 */
	public static void error(String error) {
		logger.error(error);
	}

	/**
	 * Log de debug
	 * 
	 * @param debug mensagem do log
	 */
	public static void debug(String debug) {
		logger.debug(debug);
	}
}
