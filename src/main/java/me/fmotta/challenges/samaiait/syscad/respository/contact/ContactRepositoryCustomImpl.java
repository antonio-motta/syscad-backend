package me.fmotta.challenges.samaiait.syscad.respository.contact;

import me.fmotta.challenges.samaiait.syscad.infra.base.RepositoryCustomImplBase;

/**
 * Implementação dos métodos da intefarce {@link ContactRepositoryCustom}
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
public class ContactRepositoryCustomImpl extends RepositoryCustomImplBase implements ContactRepositoryCustom {

}
