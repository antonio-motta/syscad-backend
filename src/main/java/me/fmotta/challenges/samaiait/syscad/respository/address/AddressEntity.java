package me.fmotta.challenges.samaiait.syscad.respository.address;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import me.fmotta.challenges.samaiait.syscad.infra.base.EntityBase;
import me.fmotta.challenges.samaiait.syscad.respository.person.PersonEntity;

/**
 * Entidade de endereços
 * <p>
 * Copyright (C) Motta, Antonio F. Farias - 2020
 * </p>
 * MIT License.
 */
@Entity
@Table(name = "ADDRESS")
public class AddressEntity extends EntityBase {

	/**
	 * Serialização padrão.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Entidade pessoa.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PERSON_ID", referencedColumnName = "ID", nullable = false)
	private PersonEntity person;

	/**
	 * Logradouro.
	 */
	@Column(name = "STREET")
	private String street;

	/**
	 * CEP.
	 */
	@Column(name = "CEP")
	private String cep;

	/**
	 * Bairro.
	 */
	@Column(name = "NEIGHBORHOOD")
	private String neighborhood;

	/**
	 * Cidade.
	 */
	@Column(name = "CITY")
	private String city;

	/**
	 * Estado.
	 */
	@Column(name = "UF")
	private String uf;

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(cep, city, neighborhood, person, street, uf);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressEntity other = (AddressEntity) obj;
		return Objects.equals(cep, other.cep) && Objects.equals(city, other.city)
				&& Objects.equals(neighborhood, other.neighborhood) && Objects.equals(person, other.person)
				&& Objects.equals(street, other.street) && Objects.equals(uf, other.uf);
	}

	@Override
	public String toString() {
		return "AddressEntity [person=" + person + ", street=" + street + ", cep=" + cep + ", neighborhood="
				+ neighborhood + ", city=" + city + ", uf=" + uf + "]";
	}

}
